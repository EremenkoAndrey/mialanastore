# Adonis fullstack application

## BEGIN
- run `npm install`.
- run `adonis migration:run`
- run `adonis seed` (to add admin to database)

### Start dev server

```
adonis serve --dev
```
