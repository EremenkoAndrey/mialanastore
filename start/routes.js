'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

const Route = use('Route')

Route.group(() => {
  Route.post('check-auth', 'AuthController.checkAuth')
  Route.post('login', 'AuthController.login').validator('LoginUser')
}).prefix('api/admin/v1')


Route.group(() => {
  Route.get('user', 'UserController.index')
  Route.get('user/:id', 'UserController.show')
  // Route.post('user', 'UserController.create').validator('CreateUser')
}).middleware(['auth']).prefix('api/admin/v1')
