'use strict'
const User = use('App/Models/User')

class UserController {
  async create({ request, response }) {
    try {
        return await User.create({
          username: request.body.username,
          email: request.body.email,
          password: request.body.password,
          role_name: request.body.role_name || 'user'
        });
    } catch (error) {
        response.status(500).json({errors: [{
          title: `Database error: ${error.detail}`
        }]})
    }
  }

  async index({ request }) {
    const { page = 1, perPage = 20 } = request.get();
    const result = await User
      .query()
      .paginate(page, perPage)
    return result.toJSON()
  }

  async show({ auth, params }) {
    if(params.id === 'me') {
      return auth.user
    }
  }
}

module.exports = UserController
