'use strict'

class AuthController {
  async checkAuth({ auth }) {
    return auth.check()
  }

  async login({ request, auth }) {
    const { data } = request.all()
    const { email, password } = data
    return auth.attempt(email, password)
  }

  async logout({ auth }) {
    await auth.logout()
  }
}

module.exports = AuthController
