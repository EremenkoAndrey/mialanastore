'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  toJSON(errorName) {
    return {
      errors: [{
        detail: errorName
      }]
    }
  }

  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle (error, { request, response }) {
    const format = request.accepts(['json'])
   console.log('Error handle: handle: ', error.status, error) // TODO Delete it before production
    if (format === 'json') {
      switch (error.name) {
        case 'UserNotFoundException': return response.status(error.status).json(this.toJSON('Invalid pair login/password'))
        case 'PasswordMisMatchException': return response.status(error.status).json(this.toJSON('Invalid pair login/password'))
        case 'InvalidSessionException': return response.status(error.status).json(this.toJSON('Invalid session'))
        case 'InvalidJwtToken': return response.status(error.status).json(this.toJSON('Invalid token'))
        case 'ValidationException': return response.json(error.messages)
      }
    }

    return super.handle(...arguments)
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report (error, { request }) {
  }
}

module.exports = ExceptionHandler
