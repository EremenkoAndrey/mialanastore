'use strict'

const Model = use('Model')

class Role extends Model {
  static get createdAtColumn () {
    return null
  }
  static get updatedAtColumn () {
    return null
  }
  static get primaryKey () {
    return 'name'
  }
}

module.exports = Role
