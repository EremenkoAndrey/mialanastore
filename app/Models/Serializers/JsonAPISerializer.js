'use strict'

class JsonAPISerializer {
  constructor (rows, pages = null, isOne = false) {
    this.rows = rows
    this.pages = pages
    this.isOne = isOne
  }

  first () {
    return this.rows[0]
  }

  last () {
    return this.rows[this.rows.length - 1]
  }

  size () {
    return this.isOne ? 1 : this.rows.length
  }

  _getRowJSON (modelInstance) {
    const json = modelInstance.toObject()
    const element = {
      id: json.id,
      type: modelInstance.constructor.name.toLowerCase(),
      attributes: {}
    }

    element.attributes = {};
    Object.keys(json).forEach((key) => {
        if (key === 'id') {
          return false
        }
      element.attributes[key] = json[key]
    })

    return element
  }

  _getMetaJSON () {
    const meta = {};
    if (this.pages) {
      meta.total = parseInt(this.pages.total, 10) || null
      meta.page = parseInt(this.pages.page, 10) || null
      meta.perPage = this.pages.perPage || null
      meta.lastPage = this.pages.lastPage || null
    }
    return meta
  }

  toJSON () {
    if (this.isOne) {
      return {
        data: this._getRowJSON(this.rows)
      }
    }

    return {
      meta: this._getMetaJSON(),
      data: this.rows.map(this._getRowJSON)
    }
  }
}

module.exports = JsonAPISerializer
