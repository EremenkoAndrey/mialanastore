'use strict'

class CreateUser {
  get sanitizationRules () {
    return {
      username: 'strip_tags',
      email: 'normalize_email',
      role_name: 'strip_tags'
    }
  }
  get rules () {
    return {
      username: 'required',
      email: 'required|email|unique:users',
      password: 'required',
      role_name: 'required|string|exists:roles,name'
    }
  }
}

module.exports = CreateUser
