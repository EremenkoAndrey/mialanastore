'use strict'

const { formatters } = use('Validator')

class LoginUser {
  get formatter () {
    return formatters.JsonApi
  }

  get sanitizationRules () {
    return {
      'data.email': 'normalize_email'
    }
  }
  get rules () {
    return {
      'data.email': 'required|email',
      'data.password': 'required'
    }
  }
}

module.exports = LoginUser
