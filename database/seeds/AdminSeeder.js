'use strict'
const User = use('App/Models/User')
const Role = use('App/Models/Role')

class AdminSeeder {
  async run() {
    const role = await Role.create({
      name: 'admin'
    })

    const users = new User()
    users.username = 'Admin'
    users.email = 'admin@mail.de'
    users.password = '123456'
    users.role_name = role.name
    await users.save()
  }
}


module.exports = AdminSeeder
