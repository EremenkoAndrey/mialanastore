'use strict'

const Schema = use('Schema')

class RolesSchema extends Schema {
  up () {
    this.create('roles', (table) => {
      table.string('name', 255).notNullable().unique().primary()
      table.boolean('is_default').defaultTo(false)
    })
  }

  down () {
    this.drop('roles')
  }
}

module.exports = RolesSchema
