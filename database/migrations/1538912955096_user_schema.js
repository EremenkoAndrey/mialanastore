'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('role_name', 60).references('name').inTable('roles')
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('role_name')
    })
  }
}

module.exports = UserSchema
